#!/usr/bin/env python3
import yaml
import os
import pandas as pd
from mip import *
import re
from Levenshtein import distance



# print("Program Starting..")
# time.sleep(0.5)


def load_yaml(file):
    """read  yaml in dictionary
    Get necessary Information from Frontend in yml format

    Args:
        file(str): Yaml File
    Returns:
        dict: Dictionary D
    Raises:
        YAMLError: File missing or incorrect type
    """
    print("Step 1: Reading in the Yaml File..")
    # time.sleep(0.5)
    with open(file, 'r') as stream:
        try:
            dictionary = yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print(exc)
    return dictionary


def check_path(outdir):
    """check whether desired output directory already exist
    if not the dictionary will be made

        Args:
            outdir(str): output directory
        """
    if not os.path.exists(outdir):
        os.makedirs(outdir)


def get_index(dictionary):
    """get all indices of the dictionary

    Args:
        dictionary(dict): Dictionary D
    Returns:
        list: List of all Indices
    """
    list_for_matrix = []
    for key, value in dictionary.items():
        if isinstance(value, dict):
            for key1, value1 in value['kits'].items():
                sets = key1
                for tupel, name in value1.items():
                    if "," in tupel:
                        forward_reverse = tuple(tupel.split(','))
                        for_matrix = (sets, forward_reverse, name)
                        list_for_matrix.append(for_matrix)
    return list_for_matrix


def sort_index(unsorted_index):
    """sort the index combinations

    Args:
        unsorted_index(list): List of all indices
    Returns:
        list: List of all Indices sorted by Kits
    """
    print("Step 3: Indices will be sorted depends on the Kits.")
    # time.sleep(0.5)
    sorted_list = sorted(unsorted_index, key=lambda sets: sets[0])
    taken_indices_idx = [idx for idx, i in enumerate(sorted_list) if i[2] is not None]
    return sorted_list, taken_indices_idx


def get_kit(dictionary):
    """Get all informations of Kits

    Args:
        dictionary(dict): Dictionary D from load_yaml()
    Returns:
        dict:
            - kits: Sets and their size of indices
            - kits_index_to_selected: selected sets and their size of Indices
            - kits_desired_size: selected sets and their desired sample names size
            - kits_sample_names: selected Sets and their desired sample names
    """
    kits, kits_selected, kits_desired_size = ({} for dicts in range(3))
    kits_sample_names = defaultdict(list)
    list_for_matrix = get_index(dictionary)
    for for_matrix in list_for_matrix:
        if for_matrix[0] in kits:
            kits[for_matrix[0]] += 1
        else:
            kits[for_matrix[0]] = 1
        if for_matrix[2] is None:
            if for_matrix[0] in kits_selected:
                kits_selected[for_matrix[0]] += 1
            else:
                kits_selected[for_matrix[0]] = 1
    to_select = dictionary["to_select"]["kits"]
    for key, value in to_select.items():
        kits_desired_size[key] = 0
        for key1 in value["desired_samplenames"].items():
            kits_desired_size[key] += 1
    for kit, index in to_select.items():
        for samplename, leer in index["desired_samplenames"].items():
            kits_sample_names[kit].append(samplename)
    kits_sample_names = dict(kits_sample_names)
    return kits, kits_selected, kits_desired_size, kits_sample_names


def levenshtein(seq1, seq2):
    """Levenshtein Distance Coefficient for Sequence Distance

        Args:
            seq1, seq2(str): DNA Sequences
        Returns:
            int: Distance of the Sequences
        """
    if "-" in seq1 or "-" in seq2:
        effective_seq1, effective_seq2 = seq1.rstrip("-"), seq2.rstrip("-")
        if len(effective_seq1) < len(effective_seq2):
            shorter_seq2 = seq2[:len(effective_seq1)]
            difference = distance(shorter_seq2, effective_seq1)
        else:
            shorter_seq1 = seq1[:len(effective_seq2)]
            difference = distance(shorter_seq1, effective_seq2)
    else:
        difference = distance(seq1, seq2)
    return difference


def levenshtein_old(seq1, seq2):
    """Levenshtein Distance Coefficient for Sequence Distance

    Args:
        seq1, seq2(str): DNA Sequences
    Returns:
        int: Distance of the Sequences
    """
    size_x, size_y = len(seq1) + 1, len(seq2) + 1
    matrix = np.zeros((size_x, size_y))
    for x in range(size_x):
        matrix[x, 0] = x
    for y in range(size_y):
        matrix[0, y] = y

    for x in range(1, size_x):
        for y in range(1, size_y):
            if seq1[x - 1] == seq2[y - 1] or (seq1[x - 1] == "-" or seq2[y - 1] == "-"):
                matrix[x, y] = min(
                    matrix[x - 1, y] + 1,
                    matrix[x - 1, y - 1],
                    matrix[x, y - 1] + 1)
            else:
                matrix[x, y] = min(
                    matrix[x - 1, y] + 1,
                    matrix[x - 1, y - 1] + 1,
                    matrix[x, y - 1] + 1)
    return matrix[size_x - 1, size_y - 1]


def dist(index1, index2):
    """The actual distance Coefficient for Index Combinations

    Args:
        index1, index2(tuple): Index Combinations of two DNA Sequences (Tuple)
    Returns:
        int: Levenshtein Distance of two Tuples
    """
    difference = max(levenshtein(index1[0], index2[0]), levenshtein(index1[1], index2[1]))
    return difference


def index_evenlength(index_list_sorted):
    """Fill Indices of Uneven Length (Padding)

        Args:
            index_list_sorted(list): list with Tuples of index combinations with uneven length
        Returns:
            list: list with Tuples of index combinations with even length
        """
    # print("Indices will be padded for computing the distances.")
    just_indices, tmp, filled, new_indexsequence_paired = ([] for empty_list in range(4))
    for entry in index_list_sorted:
        if isinstance(entry[1], list):
            forward, reverse = entry[1][0], entry[1][1]
        else:
            forward, reverse = entry[0], entry[1]
        just_indices.extend([forward, reverse])
    max_length = len(max(just_indices, key=len))
    tmp = [str(seperated).ljust(max_length, '-') for seperated in just_indices]
    # put forward and backward indices back together
    for k in range(len(tmp) - 1):
        if k % 2 == 0:
            temp_tuple = [tmp[k] + "," + tmp[k + 1]][0].split(',')
            new_indexsequence_paired.append(temp_tuple)

    return new_indexsequence_paired


# braucht ne weile FIX THIS SHIT
def distance_matrix(sequences):
    """Compute distance matrix

        Args:
            sequences(list): list of sequences with even length
        Returns:
            np.array: distance matrix as numpy array
        Notes:
            take a wile to compute if input arg is larger than 1000 sequences ~ 6 minutes
        """
    n = len(sequences)
    my_array = np.zeros((n, n), dtype=np.uint8)
    for i, ele_1 in enumerate(sequences):
        for j, ele_2 in enumerate(sequences):
            if j >= i:
                break
            difference = dist(ele_1, ele_2)  # Levenshtein Distance
            my_array[i, j] = difference
            my_array[j, i] = difference
    return my_array


def compute_matrix(filled_indices, sorted_tuple, threshold):
    """Set a threshold in difference matrix to a minimum distance constrained, so the
        pairwise base-distance is maximal the threshold value
        otherwise the cell value will be assigned 0

        Args:
            filled_indices(list): Sequences with even length for using distance_matrix()
            sorted_tuple(list): All Indices sorted by Kits from sort_index()
            threshold(int): Number of mismatches allowed between the index combinations
        Returns:
            pandas.DataFrame: distance matrix with row and column names
        Notes:
            take a wile to compute if input arg is larger than 1000 sequences ~ 6 minutes
        """
    matrix = distance_matrix(filled_indices)
    temp = [str(i) for i in sorted_tuple]
    dfs = pd.DataFrame(matrix, index=temp, columns=temp)
    dfs[dfs < threshold] = 0
    dfs[dfs > 0] = 1
    return dfs


def taken_collision_check(sorted_list, dictionary):
    """Check for collisions between taken indices (value in different matrix = 0)
        Args:
            sorted_list(list): All Indices sorted by Kits from sort_index()
            dictionary(dict): Dictionary D from load_yaml()
        Returns:
            bool: Description about Collision between taken indices
        Notes:
            Taken indices are "marked" and should collide between each other. After this function detected
            collisions after MILP the program will throw an warning, the program will run further.
            If the program should crash after collisions are occured the -break flag should be used.
    """
    validity = False
    taken_indices = [i for i in sorted_list if i[2] is not None]
    taken_filled = index_evenlength(taken_indices)
    threshold = dictionary["dist_threshold"]
    matrix_taken = compute_matrix(taken_filled, taken_indices, threshold)
    for i in range(len(taken_filled)):
        for j in range(i + 1, len(taken_filled)):
            if matrix_taken.iloc[i][j] == 0:
                validity = True
    return validity, taken_filled, matrix_taken


def create_content_matrix(filled_list):
    """Position Frequency Matrix for every forward indices for the nucleotide balance constraint in MILP
        Args:
            filled_list(list): Sequences with even length from index_evenlength()
        Returns:
            pandas.DataFrame: A, C, G, T Content
    """
    # print("The frequency matrix and dna base content will be computed..")
    # time.sleep(0.5)
    indices_for = [forward[0] for forward in filled_list]
    n = len(indices_for[0])
    frequency_matrix = {base: np.zeros(n, dtype=np.int8)
                        for base in 'ACGT'}
    for dna in indices_for:
        for index, base in enumerate(dna):
            if base in 'ACGT':
                frequency_matrix[base][index] += 1
    df_base = pd.DataFrame.from_dict(frequency_matrix)
    df_base['sum'] = df_base.sum(axis=1)

    df_base['A_Content'], df_base['C_Content'], df_base['G_Content'], df_base['T_Content'] = \
        df_base['A'] / df_base['sum'], \
        df_base['C'] / df_base['sum'], \
        df_base['G'] / df_base['sum'], \
        df_base['T'] / df_base['sum']
    df_base = df_base.drop(['A', 'C', 'G', 'T', 'sum'], axis=1).T
    return df_base


def milp(desired_samples, sorted_indices, diff_matrix, dictionary):
    """Mixed Integer Linear Programming Approach
        Args:
            desired_samples(dict): Kits und their amount of desired sample names
            sorted_indices(list): All Indices sorted by Kits from sort_index()
            diff_matrix(pandas.DataFrame): levenshtein distance matrix with row and column names
            dictionary(dict): Dictionary D from load_yaml()
            outfile(str): Define Output File in mlp format with each variable and constraint that will be used
        Returns:
            list: Optimal Variables as Strings
            str: Description about the fit of the model based on the input
            int: Sum of the desired sample names
        Notes:
            Whether there are collisions between taken indices, the weight of each index will be changed
    """
    # print('Step 5: building model to optimize the constrained-based selection of index combinations')
    # Define Model
    m = Model(sense=MAXIMIZE, solver_name=CBC)

    sum_of_desired = 0
    for key, value in desired_samples.items():
        sum_of_desired += value

    # Decision Variables
    n = len(sorted_indices)
    x = [m.add_var(var_type=BINARY) for i in range(n)]
    weight = 1

    # Auftreten der Nukleotidbase - n Variable
    # a

    # Constraints
    # 1) Pairwise comparison between the indices. Out of collide indices will be one index selected
    for i in range(len(sorted_indices)):
        for j in range(i + 1, len(sorted_indices)):
            if diff_matrix.iloc[i][j] == 0:
                m += x[i] + x[j] <= 1

    # 2) Marked Indices will be fixed. If they collide a weight will be defined
    if not taken_collision_check(sorted_indices, dictionary):
        for idx, i in enumerate(sorted_indices):
            if i[2] is not None:
                m += x[idx] == 1
    else:
        # define weights if taken_collision_check = True
        weight = [(sum_of_desired + 1) if i[2] is not None else 1 for idx, i in enumerate(sorted_indices)]

    # 3) Aus den jeweiligen Sets werden so viele Indices genommen, wie erwünscht (variable)
    for key, value in desired_samples.items():
        tmp = [idx for idx, j in enumerate(sorted_indices) if j[0] == key if j[2] is None]
        m += xsum(x[i] for i in tmp) == value

    ''' # Warning falls dies nicht genau ausgeglichen ist, falls nicht genug umsetzbar
    # 3) gewähltes set sollte möglichst ausbalanciert sein(Basen)
    # In der Constraint kann man einen Cutoff wählen, falls es keine Lösung gibt, gibt er keine aus
    # maximale Abweichung in beide Richtungen für eine (oder alle) Basen (nur dann wird die Constraint aktiviert)
    '''

    # Objective
    if isinstance(weight, int):  # wenn es zu keiner kollision bei taken kommt, bleibt weight konstant
        # Nucleotid balance könnte auch eine Lösung geben, die nicht optimal sein muss
        m.objective = maximize(xsum(weight * x[i] for i in range(n)))
    elif isinstance(weight, list):  # wenn es zu kollision bei taken kommt, wird weight zu einer liste mit gewichtung
        m.objective = maximize(xsum(weight[i] * x[i] for i in range(n)))

    # print('model has {} vars, {} constraints and {} nzs'.format(m.num_cols, m.num_rows, m.num_nz))

    status = m.optimize()

    # m.write(f'results/model/model_{outfile}.lp')

    # print the model
    # print(f"status: {m.status}")
    # print(f"objective: {m.objective_value}")

    opt_var = []
    if status == OptimizationStatus.OPTIMAL:
        print('optimal solution cost {} found'.format(m.objective_value))
    elif status == OptimizationStatus.FEASIBLE:
        print('sol.cost {} found, best possible: {} '.format(m.objective_value, m.objective_bound))
    elif status == OptimizationStatus.NO_SOLUTION_FOUND:
        print('no feasible solution found, lower bound is: {} '.format(m.objective_bound))
    if status == OptimizationStatus.OPTIMAL or status == OptimizationStatus.FEASIBLE:
        print('solution:')
        for v in m.vars:
            if abs(v.x) > 1e-6:  # only printing non-zeros
                print('{} : {} '.format(v.name, v.x))
                opt_var.append(v.name)
    return opt_var, status


def create_outyaml(optimal_variables, dictionary, sorted_indices, sample_names):
    """Create Output Yaml if optimal variables exists for frontend
            Args:
                optimal_variables(list): Optimal Variables from MILP model
                dictionary(dict): Dictionary D from load_yaml()
                sorted_indices(list): All Indices sorted by Kits from sort_index()
                sample_names(dict): Information about Kits and their size of desired sample names
            Returns:
                dict: Taken Indices + Optimal Variables from MILP model
                str: File Path of the result in Yaml format
        """
    print(
        f'A optimal solution was found. {sample_names} will be added to marked indices..')
    # time.sleep(0.5)
    # -----------------------------------
    # print('Optimal Variables will be hashed..')
    index_list = []
    for i in optimal_variables:
        digit = int(re.search(r'\d+', i).group())
        index_list.append(digit)
    # print("Index Liste: ", index_list, " mit der Länge: ", len(index_list))

    result = {'taken_indices': {'kits': {}}, 'selected': {'kits': {}}}
    marked_indices = dictionary["taken_indices"]
    result["taken_indices"] = marked_indices
    decided = []

    kits_indexvariables = defaultdict(list)
    for idx, tupel in enumerate(sorted_indices):
        for variable in index_list:
            if idx == variable:
                decided.append(tupel[1])
                if tupel[2] is None:
                    for key, value in sample_names.items():
                        if tupel[0] == key:
                            for_rev = ','.join(tupel[1])
                            kits_indexvariables[key].append(for_rev)
    kits_indexvariables = dict(kits_indexvariables)

    for kit, variable in kits_indexvariables.items():
        result["selected"]["kits"][kit] = dict(zip(variable, sample_names[kit]))
        
    return result, decided


