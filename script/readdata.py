#!/usr/bin/env python3
# Libraries
import pandas as pd
import json
from collections import defaultdict
from mergedeep import merge
import random
import collections
import yaml
import numpy as np
from sample_sheet import SampleSheet
from sample_sheet import Sample
import csv 

def levenshtein(seq1, seq2):
    """
    calculate levenshtein distance for two strings (indices)
    :param seq1: first string index
    :param seq2: second string index
    :return: distances of indices
    """
    size_x = len(seq1) + 1
    size_y = len(seq2) + 1
    matrix = np.zeros((size_x, size_y))
    for x in range(size_x):
        matrix[x, 0] = x
    for y in range(size_y):
        matrix[0, y] = y

    for x in range(1, size_x):
        for y in range(1, size_y):
            if seq1[x - 1] == seq2[y - 1] or (seq1[x - 1] == "-" or seq2[y - 1] == "-"):
                matrix[x, y] = min(
                    matrix[x - 1, y] + 1,
                    matrix[x - 1, y - 1],
                    matrix[x, y - 1] + 1
                )
            else:
                matrix[x, y] = min(
                    matrix[x - 1, y] + 1,
                    matrix[x - 1, y - 1] + 1,
                    matrix[x, y - 1] + 1
                )
    return matrix[size_x - 1, size_y - 1]


def display_kit(kit):
    """
    Display content of selected file. Selected files are here different index files with different kits.
    :param kit: csv File which include the indices and kit name
    :return: filtered indices and number of entries from file
    """

    file = pd.read_csv(kit, header=None) #new without sep="\n"
    start = 0
    end = 0
    data = []
    for i in range(0, len(file)):
        if file[0][i] == "[Indices]":
            start = i + 2  # don't the header in the list only index sequence
        if file[0][i] == "[SupportedModules]":
            end = i
    indices = file[0][start:end] + "\t" + " "  # add an extra column for sample names

    for i in range(start, end):  # have all information transform into correct dataframe
        data.append([splits for splits in indices[i].split("\t") if splits != ""])

    result = data
    entries = len(data)
    return result, entries


# new
def create_dataframe2(data, kitname, treshold):
    """
    Create the dataframe for selected and uploaded samplesheet in r-shiny app.
    Data will be display in a dictionary

    :param data: table that has bin created or uploaded into shiny app
    :param kitname: name of set from samplesheet
    :param treshold: threshold the user has selected via shiny app
    :return: dictionary of selected or created input
    """

    # first step filter the data: sample-id, forward index and backward index
    id = data['sampleSheet.Sample_ID'].to_list()
    backward = data["sampleSheet.index2"]
    forward = data["sampleSheet.index"]

    # create the structure for dataframe
    dict_nested = lambda: defaultdict(dict_nested)
    dict_kit = dict_nested()
    # forw_and_back = forward[0] + "," + backward[0]

    # safe data from the customized table, if no samplesheet was uploaded
    if kitname is None:
        kit_name_list = data["Info"].to_list()  # retreat Info column samplesheet name/ used kit, has to be given
        for i in range(len(forward)):
            if len(forward[i]) == 0:  # if no index is given replace it with "-"
                forward[i] = '-'
            if len(backward[i]) == 0:
                backward[i] = '-'
            forw_and_back = forward[i] + "," + backward[i]
            kit_name = kit_name_list[i]
            dict_kit["taken_indices"]["kits"][kit_name][forw_and_back] = id[i]
        dict_kit["dist_threshold"] = treshold
    else:

        # kit_name = kitname.split('.csv')[0]
        kit_name_list = data["filename"].to_list()  # new

        # if we got the input files (samplesheets uploaded) and table set, safe the data
        # dict_kit["taken_indices"]["kits"][kit_name][forw_and_back] = id[0]
        for i in range(len(forward)):
            if len(forward[i]) == 0:
                forward[i] = '-'
            if len(backward[i]) == 0:
                backward[i] = '-'
            forw_and_back = forward[i] + "," + backward[i]
            kit_name = kit_name_list[i]  # new
            dict_kit["taken_indices"]["kits"][kit_name][forw_and_back] = id[i]

        dict_kit["dist_threshold"] = treshold

    return dict_kit


def create_dataframe(data, kitname, treshold):
    """
    Create the dataframe for selected and uploaded samplesheet in r-shiny app.
    Data will be display in a dictionary

    :param data: table that has bin created or uploaded into shiny app
    :param kitname: name of set from samplesheet
    :param treshold: threshold the user has selected via shiny app
    :return: dictionary of selected or created input
    """

    # first step filter the data: sample-id, forward index and backward index
    id = data['sampleSheet.Sample_ID'].to_list()
    backward = data["sampleSheet.index2"]
    forward = data["sampleSheet.index"]

    # create the structure for dataframe
    dict_nested = lambda: defaultdict(dict_nested)
    dict_kit = dict_nested()
    # forw_and_back = forward[0] + "," + backward[0]

    # safe data from the customized table, if no samplesheet was uploaded
    if kitname is None:
        kit_name_list = data["Info"].to_list()  # retreat Info column samplesheet name/ used kit, has to be given
        for i in range(len(forward)):
            if len(forward[i]) == 0:  # if no index is given replace it with "-"
                forward[i] = '-'
            if len(backward[i]) == 0:
                backward[i] = '-'
            forw_and_back = forward[i] + "," + backward[i]
            kit_name = kit_name_list[i]
            dict_kit["taken_indices"]["kits"][kit_name][forw_and_back] = id[i]
        dict_kit["dist_threshold"] = treshold
    else:
        kit_name = kitname.split('.csv')[0]

        # if we got the input files (samplesheets uploaded) and table set, safe the data
        # dict_kit["taken_indices"]["kits"][kit_name][forw_and_back] = id[0]
        for i in range(len(forward)):
            if len(forward[i]) == 0:
                forward[i] = '-'
            if len(backward[i]) == 0:
                backward[i] = '-'
            forw_and_back = forward[i] + "," + backward[i]
            dict_kit["taken_indices"]["kits"][kit_name][forw_and_back] = id[i]

        dict_kit["dist_threshold"] = treshold

    return dict_kit


def create_fasta_format(dictionary, filename):
    """
    Create a FASTA file for download of the results from ilp
    :param dictionary: result from ilp as a dictionary
    :param filename: set a name for the file
    :return: FASTA file
    """
    forward = list()
    backward = list()

    # get all indices from file
    for kit, index in dictionary["selected"]["kits"].items():
        for index_selected, value in index.items():
            # append all indices from selected
            forward.append(index_selected.split(',')[0])
            backward.append(index_selected.split(',')[1])

    for kit, index in dictionary["taken_indices"]["kits"].items():
        for index_taken_indices, value in index.items():
            # append all indices from taken indices
            forward.append(index_taken_indices.split(',')[0])
            backward.append(index_taken_indices.split(',')[1])

    # write fasta file format
    ofile = open(filename, "w")

    for index in range(len(forward)):
        if forward[index] != "-":
            ofile.write(">" + "i" + str(index) + "forward" + "\n" + forward[index] + "\n")
        if backward[index] != "-":
            ofile.write(">" + "i" + str(index) + "backward" + "\n" + backward[index] + "\n")

    ofile.close()

'''
def create_samplesheet_output(dictionary, filename):
    sample_sheet = SampleSheet()
    sample_sheet.Header['Investigator Name'] = 'User'

    forward = list()
    backward = list()
    sample_name = list()

    # get all indices from file
    for kit, index in dictionary["selected"]["kits"].items():
        for index_selected, value in index.items():
            sample_name.append(value)
            # append all indices from selected
            if index_selected.split(',')[0] == '-':
                forward.append("")
            else:
                forward.append(index_selected.split(',')[0])

            if index_selected.split(',')[1] == '-':
                backward.append("")
            else:
                backward.append(index_selected.split(',')[1])

    for kit, index in dictionary["taken_indices"]["kits"].items():
        for index_taken_indices, value in index.items():
            sample_name.append(value)

            if index_taken_indices.split(',')[0] == '-':
                forward.append("")
            else:
                forward.append(index_taken_indices.split(',')[0])

            if index_taken_indices.split(',')[1] == '-':
                backward.append("")
            else:
                backward.append(index_taken_indices.split(',')[1])

    for i in range(len(forward)):
        sample = Sample(
            dict(Sample_ID=sample_name[i], Sample_Name=sample_name[i], index=forward[i], index2=backward[i]))
        # print(sample)
        sample_sheet.add_samples(sample)

    # Write Samplesheet
    sample_sheet.write(open(filename, 'w'))
'''

################################################################################

def create_samplesheet_output2(dictionary, filename):

    forward = list()
    backward = list()
    sample_name = list()

    # get all indices from file
    for kit, index in dictionary["selected"]["kits"].items():
        for index_selected, value in index.items():
            sample_name.append(value)
            # append all indices from selected
            if index_selected.split(',')[0] == '-':
                forward.append(" ")
            else:
                forward.append(index_selected.split(',')[0])

            if index_selected.split(',')[1] == '-':
                backward.append(" ")
            else:
                backward.append(index_selected.split(',')[1])

    for kit, index in dictionary["taken_indices"]["kits"].items():
        for index_taken_indices, value in index.items():
            sample_name.append(value)

            if index_taken_indices.split(',')[0] == '-':
                forward.append(" ")
            else:
                forward.append(index_taken_indices.split(',')[0])

            if index_taken_indices.split(',')[1] == '-':
                backward.append(" ")
            else:
                backward.append(index_taken_indices.split(',')[1])

        
    csvfile = open(filename, 'w', newline='', encoding='utf-8')
    c = csv.writer(csvfile)
    
    # write a column headings row - do this only once -
    c.writerow(['[Header]'])
    c.writerow('\n')
    c.writerow(['[Reads]'])
    c.writerow('\n')
    c.writerow(['[Settings]'])
    c.writerow('\n')
    c.writerow(['[Data]'])
    
    c.writerow( ['Sample_ID', 'Sample_Name', 'index', 'index2'] )
    the_list = list()
    for i in range(len(forward)):
        the_list.append([sample_name[i],sample_name[i], forward[i], backward[i]])
    
    # use a for-loop to write each row into the CSV file
    for item in the_list:
        # write one row to csv — item MUST BE a LIST
        c.writerow(item)
    
    
    # save and close the file
    csvfile.close()





################################################################################

def get_reverse_index(dictionary):
    """
    Will get the reverse index from a dictionary (also safe the forward to get back the index_pair)
    :param dictionary: dictionary from ilp
    :return: list of backward and forward indices
    """

    backward_taken_indices = list()
    forward_taken_indices = list()
    for kit, index in dictionary["taken_indices"]["kits"].items():
        for index_taken_indices, value in index.items():
            forward_taken_indices.append(index_taken_indices.split(',')[0])
            backward_taken_indices.append(index_taken_indices.split(',')[1])

    return backward_taken_indices, forward_taken_indices


def dictionary_reverse_complement(dictionary, index_pair):
    """
    Will bring the reverse-index back to its reverse complement. So it can be downloaded into a fastq file
    :param dictionary: dictionary from the results of the ilp
    :param index_pair: reverse and forward indices
    :return: dictionary
    """
    for kit, index in list(dictionary["taken_indices"]["kits"].items()):
        index_count = 0
        for index_taken_indices, value in list(index.items()):
            # forward = index_taken_indices.split(',')[0]
            dictionary["taken_indices"]["kits"][kit][index_pair[index_count]] = \
            dictionary["taken_indices"]["kits"][kit][index_taken_indices]
            del dictionary["taken_indices"]["kits"][kit][index_taken_indices]
            index_count += 1

    return dictionary


def create_dataframe_tsv(data, kitname, treshold, selected_samplenames, index_number, kit_entries):
    """
    Create separate dataframe for the tsv files/ index files that include different kits
    Data is also saved in a dictionary.

    :param data: table with selected input for index file
    :param kitname: kit name
    :param treshold: user input selected threshold
    :param selected_samplenames: selected samplenames for each index (from table)
    :param index_number: number of indices the user wants to selected from each index file
    :param kit_entries: counts the number of indices listed in selected kit
    :return: dictionary for selected input
    """
    # create the structure for dataframe
    dict_nested = lambda: defaultdict(dict_nested)
    dict_kit = dict_nested()
    dict_kit_samplename = dict_nested()  # structure for sample names
    forward = list()
    backward = list()

    # get the set/kit name
    if kitname is None:
        kit_name = "new_kit"
    else:
        kit_name = kitname

    # separate forward and backward indices
    for i in range(len(data)):
        if data[i][2] == '1':
            forward.append(data[i][1])
        else:
            backward.append(data[i][1])

    # check if forward is empty or backward is empty
    if len(backward) == 0:
        backward.append("-")
    if len(forward) == 0:
        forward.append("-")

    # combine forward and backward indices: all possible combinations, add them to dictionary
    for i in range(len(forward)):
        for j in range(len(backward)):
            forw_and_back = forward[i] + "," + backward[j]
            dict_kit["to_select"]["kits"][kit_name][forw_and_back] = None

    # desired sample names sets the number of indices I wanna select, add to dictionary
    for j in range(index_number):

        if index_number > kit_entries:  # if we want to select more indices than displayed in table
            create_sample_name = "sample_" + str(j)  # add a samplename independently from given samplenames of table
            dict_kit["to_select"]["kits"][kit_name]["desired_samplenames"][create_sample_name] = None
        else:
            name = selected_samplenames[str(j)][3]
            if name == " ":
                name = "sample_" + str(j)  # add a samplename if none has been added
            dict_kit["to_select"]["kits"][kit_name]["desired_samplenames"][name] = None

    # add given samplename from table
    for k in range(len(data)):
        samplename = selected_samplenames[str(k)][3]
        dict_kit_samplename["to_select"]["kits"][kit_name]["desired_samplenames"][samplename] = None

    dict_kit["dist_threshold"] = treshold

    return dict_kit, dict_kit_samplename, backward


def merge_dict(data, number_of_inputs):
    """
    :param data: selected kits data from display_kit
    :param number_of_inputs: number of selected index_files
    :return: merged dictionary of all index_files
    """

    letters = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15']
    dict_nested = lambda: defaultdict(dict_nested)
    result = dict_nested()

    for i in range(number_of_inputs):
        if letters[i] in data.keys():
            result = merge(result, data[letters[i]])
        else:
            result

    return result


def write_json(result, name):
    """
    Write dictionary into json file
    :param result: dictionaries from samplesheet and index files
    :param name: file to be named
    :return: json file
    """
    json_str = json.loads(json.dumps(result))
    with open(name, 'w') as outfile:
        outfile.write(json.dumps(json_str))


def max_hamming(nested):
    """
    Recursive function for iterating through the dict
    :param nested: nested dictionary
    """
    for key, value in nested.items():
        if isinstance(value, collections.Mapping):
            for inner_key, inner_value in max_hamming(value):
                yield inner_key, inner_value
        else:
            yield key, value


def merge_all(dict1, dict2):
    """
    Merge two dictionaries
    :param dict1: dictionary for taken_indices = samplesheet
    :param dict2: dictionary fot to_select = index files
    :return: merged dictionary
    """
    dict2.update(dict1)
    return dict2


def write_yaml(data, filename):
    """
    Write result from merged dictionaries into to a yaml file
    :param data: merged dictionary from merge_all
    :param filename: name of yaml file (user input)
    :return: yaml file
    """
    with open(filename, 'w') as f:
        yaml.dump(json.loads(json.dumps(data)), f, default_flow_style=False)


def validity_check(result_dict, threshold, filename):
    """
    Check for collisions (in selected samplesheet) to avoid crashes
    :param result_dict: dictionary for samplesheet
    :param threshold: user input threshold
    :param filename: filename of uploaded samplesheet
    :return: list of colliding indices
    """
    collisions = list()
    collisions_names = list()
    collisions_filename = list()
    indices = list()
    sample = list()
    all_indexsequence = list()
    new_indexsequence = list()
    new_indexsequence_paired = list()
    result = list(max_hamming(result_dict))

    for i in range(len(result)):
        if ',' in result[i][0]:
            indices.append(result[i][0])  # get indices
            sample.append(result[i][1])  # get names

    # calculate distances and check if they are under the threshold if yes we have a collisions
    # first step get indices
    for n in range(len(indices)):
        all_indexsequence.append(indices[n].split(',')[0])
        all_indexsequence.append(indices[n].split(',')[1])
    maximum_length = len(max(all_indexsequence, key=len))
    # set all indices into the same length
    for sequence in all_indexsequence:
        for i in range(maximum_length - len(sequence)):
            sequence += '-'
        new_indexsequence.append(sequence)
    # put forward and backward indices back together
    for k in range(len(new_indexsequence) - 1):
        if k % 2 == 0:
            temp_tuple = (new_indexsequence[k] + "," + new_indexsequence[k + 1])
            new_indexsequence_paired.append(temp_tuple)

    # calculate the levenshtein distance for index sequences
    max_distance = np.zeros(
        (len(new_indexsequence_paired), len(new_indexsequence_paired)))  # initialize matrix with number index sequences
    for i in range(len(new_indexsequence_paired)):
        for j in range(len(new_indexsequence_paired)):
            forward = levenshtein(new_indexsequence_paired[i].split(',')[0],
                                  new_indexsequence_paired[j].split(',')[0])  # forward
            backward = levenshtein(new_indexsequence_paired[i].split(',')[1],
                                   new_indexsequence_paired[j].split(',')[1])  # backward
            max_distance[i][j] = max(forward, backward)
            if i != j:  # don't get the zero diagonals
                if max_distance[i][j] < threshold:  # check for collision and save them
                    if new_indexsequence_paired[i] not in collisions:  # don't safe an index combination twice
                        collisions.append(new_indexsequence_paired[i])
                        collisions_names.append(str(sample[i]))
                        collisions_filename.append(filename)

    return collisions, max_distance, pd.DataFrame(zip(collisions, collisions_names, collisions_filename))
